#### windows-10-kiosk-chrome-autostart

using windows PowerShell to run a rendered app , 
needs /index.html in windows-10-chrome-kiosk/web/

## Install 

copy your web project into `windows-10-chrome-kiosk/web/`

then copy the whole `windows-10-chrome-kiosk/` folder somewhere ( e.g. Desktop )

find the autostart folder:
1. <kbd>command</kbd> + <kbd>r</kbd>

2. enter "shell:startup"

3. link "powerstart.bat" to your autostart folder either by dragging with <kbd>ALT</kbd> pressed or dragging with right mouse button---

<a href="https://the-foundation.gitlab.io/">
<h3>A project of the foundation</h3>
<div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/windows-10-kiosk-chrome-autostart/README.md/logo.jpg" width="480" height="270"/></div></a>
